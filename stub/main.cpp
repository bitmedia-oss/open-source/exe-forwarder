#ifndef UNICODE
#define UNICODE
#endif 

#include <windows.h>

WCHAR* GetConfigFileName() {
	HMODULE hModule = GetModuleHandleW(NULL);
	WCHAR* path = new WCHAR[MAX_PATH];
	GetModuleFileNameW(hModule, path, MAX_PATH);
	int p = wcslen(path);
	wcscpy_s(path + p - 3, 4, L"txt");
	return path;
}

WCHAR* GetConfig(const WCHAR* filename) {
	HANDLE hFile = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile == INVALID_HANDLE_VALUE) {
		return NULL;
	}
	int bufferSize = 16000;
	CHAR* utf8Content = new CHAR[bufferSize];
	WCHAR* content = new WCHAR[bufferSize];
	content[0] = 0;
	DWORD dwSize;
	if (ReadFile(hFile, utf8Content, bufferSize, &dwSize, NULL)) {
		if (dwSize < bufferSize) {
			utf8Content[dwSize] = 0;
			DWORD chars = MultiByteToWideChar(CP_UTF8, 0, utf8Content, dwSize, content, bufferSize);
			content[chars] = 0;
		}
	}
	CloseHandle(hFile);
	return content;
}

const WCHAR* skipCRLF(const WCHAR* src) {
	while (*src) {
		if (*src == L'\r' || *src == L'\n') {
			++src;
			continue;
		}
		break;
	}
	return src;
}

const WCHAR* findCRLF(const WCHAR* src) {
	const WCHAR* CR = wcschr(src, L'\r');
	const WCHAR* LF = wcschr(src, L'\n');
	if (!CR && !LF) {
		return src + wcslen(src);
	}
	if (CR) {
		return CR;
	}
	if (LF) {
		return LF;
	}
	return NULL;
}
int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{
	WCHAR* configFileName = GetConfigFileName();
	WCHAR* config = GetConfig(configFileName);
	if (!config) {
		MessageBoxW(NULL, L"Unable to find forwarding info", L"Exe Forwarder", MB_OK | MB_ICONERROR);
		return 1;
	}

	const WCHAR* src_forwardTo = skipCRLF(config);
	const WCHAR* src_forwardToEnd = findCRLF(src_forwardTo);

	size_t forwardToSize = src_forwardToEnd - src_forwardTo;
	WCHAR* forwardTo = new WCHAR[forwardToSize + 1];
	wcsncpy_s(forwardTo, forwardToSize + 1, src_forwardTo, forwardToSize);
	STARTUPINFO si;
	ZeroMemory(&si, sizeof(si));
	PROCESS_INFORMATION pi;
	ZeroMemory(&pi, sizeof(pi));
	DWORD dwCmdLineSize = wcslen(pCmdLine) + wcslen(forwardTo) + 10;
	WCHAR* cmdLine = new WCHAR[dwCmdLineSize];
	wcscpy_s(cmdLine, dwCmdLineSize, L"");
	wcscat_s(cmdLine, dwCmdLineSize, L"\"");
	wcscat_s(cmdLine, dwCmdLineSize, forwardTo);
	wcscat_s(cmdLine, dwCmdLineSize, L"\" ");
	wcscat_s(cmdLine, dwCmdLineSize, pCmdLine);
	if (!CreateProcess(NULL, cmdLine, NULL, NULL, FALSE, NULL, NULL, NULL, &si, &pi)) {
		MessageBoxW(NULL, L"Unable to launch forwarding exe", L"Exe Forwarder", MB_OK | MB_ICONERROR);
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
		return 1;
	}
	WaitForSingleObject(pi.hProcess, INFINITE);
	DWORD dwExitCode;
	GetExitCodeProcess(pi.hProcess, &dwExitCode);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
	return dwExitCode;
}